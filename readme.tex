\documentclass[a4paper]{article}
\usepackage{booktabs,hyperref,multirow,xltxtra}

\title{The \texttt{ntnuthesis} \LaTeX\ class, version 1.0}
\author{Federico Zenith}
\date{}

\begin{document}

\maketitle

\begin{abstract}
This document describes the \texttt{ntnuthesis} class for \LaTeX, a class for
PhD theses at the Norwegian University of Science and Technology of Trondheim.
It describes how to install and use the class.
\end{abstract}

\section*{Short Story}
See table \ref{tab:commands} to configure the document.
Use \XeLaTeX\ to compile.

\section*{``What is \LaTeX?''}
If you are lost and are not sure about what you are reading, \LaTeX\ is a
very good program to write books, articles and letters. It is different from
programs like Microsoft Word or LibreOffice Writer in that you do not specify 
the overall layout by yourself, but let the a program, \TeX, do it for 
you.

\LaTeX\ is a series of high-level functions that build on \TeX, which is instead 
a low-level typesetting system. Since \LaTeX\ has been programmed by experts 
that did the typesetting job for you, the document will look professional and 
consistent, and you may concentrate on \emph{what} to write rather than 
\emph{how} to write it. \TeX\ is particularly good at typesetting mathematics, 
and is often used in scientific journals and textbooks, and has a huge
collection of extension (called \emph{packages}) for many other uses.

Writing with \LaTeX\ requires however some learning: it is not a 
What-You-See-Is-What-You-Get word processor, as those you may be used to.
Textbooks are available at most libraries and bookstores, including the 
\href{http://ask.bibsys.no/ask/action/result?kilde=biblio&q=latex+typesetting}
{university library}; you can also check out many resources on the Internet, 
such as \href{http://en.wikibooks.org/wiki/LaTeX}{Wikibooks}. 
You will probably need about two weeks to acquire a usable basis of knowledge, 
from which you will gradually learn on with time.

\LaTeX\ is not only excellent software, it is also free. 
You can download \href{http://www.tug.org/texlive/}{\TeX\ Live} for Linux, Mac 
and Windows systems.

\section*{Description of the Class}
The \texttt{ntnuthesis} class provides a subset of the \texttt{book} class 
suitable for doctoral theses written according to \textsc{Ntnu} standards 
regarding title page, colophon, headers, margins, font size and so on.
This class uses the B5 paper size directly, allowing you to have a correct idea
of the final size and number of pages of your document. If you had used A4, 
readability of text and figures might be reduced as the pages were shrunk to B5,
or, alternatively, layout would have been completely broken if you changed the
size of fonts and other elements relative to the paper.

\subsection*{Options}
The class accepts four options:
\begin{description}
\item[\texttt{draft}] This option is passed directly to \texttt{ntnuthesis}'s 
parent class, \texttt{book}. It highlights bad boxes, does not include graphics
(but shows the area where they would be), and deactivates hyperlinks (if the 
\texttt{hyperref} package was used). If you have very cumbersome images, the 
resulting file's size will be reduced as they are not included.
Compilation is sped up and viewing is also faster.

\item[\texttt{final}] Also passed directly to \texttt{book}, is the opposite of
\texttt{draft} and is set by default.

\item[\texttt{a4crop}] This option that will print the B5 output centered on an
A4 page with cropping marks. The layout of the resulting \textsc{pdf} may be
incorrect if you compile the document along the route of
\LaTeX$\rightarrow$\texttt{dvips}$\rightarrow$\texttt{ps2pdf}. 
In that case, pass the global option \texttt{[dvips]} to the class. PDF\LaTeX\ 
works generally well without further modifications, and is better tested.

\item[\texttt{screen}] This option will output the document in a 5:4 ratio, 
suitable for viewing on a computer screen or a projector. 
If you have large pictures that take up a whole page, or any other part of the 
\LaTeX\ source that should be written differently depending on which version 
(print or screen) you are preparing, you can use the \verb|\printandscreen{}{}| 
command, a custom command defined by the class: its first option is for the 
printed version, the second one for the screen version.
\begin{verbatim}
\printandscreen{
    code for printed mode
}{
    code for screen mode
}
\end{verbatim}
An usage example may be code such as:
\begin{verbatim}
As stated on \printandscreen{page}{slide} \pageref{...},
\end{verbatim}
Here, you will automatically get the right word (``page'' or ``slide'') 
according to the build option.

The \texttt{screen} option is particularly appropriate for 
\href{http://www.ntnu.no/ub/diva/doktoravh}{electronic publication} of your 
thesis.
\end{description}

Note that you \emph{can} use \texttt{a4crop} and \texttt{screen} together, but 
it does not really make much sense to print to A4 something meant for a computer 
screen.

\subsection*{Installation}
Simply copy the class file \texttt{ntnuthesis.cls} and the NTNU logo, 
\texttt{NTNUlogo.pdf}, in a folder seen by the \TeX\ engine and update the 
internal database:
\begin{description}
\item[UNIX:] copy in a subfolder of \verb|~/texmf/tex/latex| and run
\verb|texhash ~/texmf|;
\item[Windows:] copy in an appropriate subfolder of your user, typically something like 
\verb|C:\Users\johndoe\texmf\tex\latex\|, select Run from the start menu and
type \texttt{texhash}.
\end{description}

\subsection*{Usage}
Start your \LaTeX\ file with \verb|\documentclass{ntnuthesis}|, and write
on as you would for a \texttt{book} class.

Packages can be added in the usual way, and the same goes
for the placement of \verb|\tableofcontents| and other commands.

To compile the file, you need to use \XeLaTeX, since it uses the 
\texttt{fontspec} package.
Note that some popular packages need to be replaced, usually with good reasons
and by an improved version: notably, \texttt{babel} is replaced in \XeLaTeX\ by
\texttt{polyglossia}.

\subsubsection*{Setting title and colophon}
To set the thesis' title and colophon pages (usually the first two), you will 
have to give some commands in the document's preamble to specify title, 
subtitle, author, and so on. If you do not specify at least author and title, 
issuing \verb|\maketitle| will cause \LaTeX\ to crash on compilation.  
For a full reference of all available commands, see table
\ref{tab:commands}.

\begin{table}
\begin{center}
\begin{tabular}{ccp{0.35\textwidth}}
\toprule
Field & Command & Additional information\\
\toprule
Author & \verb|\author{}|\\
\midrule
Title &  \verb|\title{}|\\
\midrule
Subtitle & \verb|\subtitle{}|\\
\midrule
Degree type &  \verb|\degreetype{}| & 
Such as \emph{doctor artium}, \emph{doctor scientiarum}, etc.\\
\midrule
Faculty &  \verb|\faculty{}|\\
\midrule
Department &  \verb|\department{}|\\
\midrule
Copyright notice & \verb|\copyrightnotice{}| & 
A basic notice is generated automatically, as: \emph{\copyright\ 1980 Umberto 
Eco.} Any text inserted with this command follows.\\
\midrule
\textsc{Isbn}, electronic &  \verb|\isbnelectronic{}| & 
\multirow{3}{*}{To be ordered \href{http://sats.itea.ntnu.no/isbnprovider/start.do}{here}.}\\
\cmidrule{1-2}
\textsc{Isbn}, printed &  \verb|\isbnprinted{}|\\
\cmidrule{1-2}
Serial number &  \verb|\serialnumber{}|\\
\midrule
Month &  \verb|\setmonth{}| & If unset, it defaults to the current month.\\
\midrule
Year &  \verb|\setyear{}| & If unset, it defaults to the current year.\\
\midrule
Publisher &  \verb|\setpublisher{}| & E.g. NTNU-Trykk or Tapir Uttrykk.\\
\bottomrule
\end{tabular}
\end{center}

\caption{\label{tab:commands}The commands used to setup the title and
colophon pages. The class takes care of the \textsc{issn} number by itself.}
\end{table}


\subsubsection*{Miscellaneous tips}
If you want to use the common pattern with roman lowercase numbers for pages up
to the first chapter (acknowledgements, abstract, table of contents, etc.), 
write \verb|\frontmatter| at the very beginning. When you want to revert to 
Arabic numbers for the thesis' chapters and appendices, write 
\verb|\mainmatter|. To deactivate chapter numbering at the end (for index and 
bibliography), use \verb|\backmatter|.

If you have to write any appendices, use the command \verb|\appendix| just 
before writing them. This will change the chapter numbering appropriately.

Figures in \LaTeX\ are the subject of many articles and books, so look these up 
for details. Remember to use the right format for your figure: vector graphics 
(\texttt{pdf}) for diagrams, non-lossy bitmap (\texttt{png}) for images with
sharp edges such as scanned diagrams, and lossy bitmap (\texttt{jpg}) only for 
photographic images. Use vector graphics whenever possible, since it does not 
lose quality with zooming and usually has a much smaller file size.

Most of the times, there are specific \LaTeX\ packages for whatever your needs 
may be. 
Search \href{http://ctan.org/}{CTAN} before trying to fix things yourself, as it
may save you a lot of effort and will generally provide a much better result. 
You will be probably interested in at least some of the following packages:
\begin{itemize}
\item[\texttt{ams*}] The \texttt{amsfonts}, \texttt{amsmath}, \texttt{amssymb} 
and \texttt{amsthm} packages are provided by the American Mathematical Society 
and provide many useful additional features related (you guessed) to mathematics 
typesetting.

\item[\texttt{makeidx}] Allows to build a thesis index automatically, provided 
you place keys here and there in the text.

\item[\texttt{listings}] Print program code, if you want to add it. Almost any 
programming language is supported.

\item[\texttt{polyglossia}] Most theses are written in English, but should you 
use another language you have to use the Babel package, so that the 
automatically  generated elements of text are properly translated and 
hyphenation patterns are correct.

\item[\texttt{fontspec}] This package is loaded already by the class to handle
UTF-8 issues such as Norwegian names containing ø, æ and å.
Furthermore, it makes it easy to change the default fonts; the default serif
font is \TeX\ Gyre Termes, a well-supported clone of Times.

\item[\texttt{booktabs}] Professional tables, a bit better looking than the 
\LaTeX\ default.

\item[\texttt{sistyle}] Handle automatically the formatting of numbers and 
units, to avoid mistakes that would be difficult to catch by hand.

\item[\texttt{hyperref}] With no extra effort to you, this package will
automatically fill the resulting \textsc{pdf} with hyperlinks throughout the 
document. It will be possible to jump directly to a reference, figure, table, 
or any referred object from any part of the text where the reference is.

\item[\texttt{epigraph}] In case you want to add some wise, insightful or just
funny quote at the beginning of a chapter, you can use this package.

\item[\texttt{chapterbib}] This is the package you need if you want to insert a 
bibliography at the end of each chapter, instead of a single one at the end of 
the thesis. If you use this together with \texttt{hyperref}, you \emph{must} 
also use the \texttt{natbib} package to resolve some issues.

\item[\texttt{tikz}] A very well-written and powerful package to draw diagrams.
Excellent documentation is available.

\item[\texttt{pgfplots}] Based ot TikZ, allows most kind plots you may want
(even some 3-D) directly in \LaTeX, keeping consistent font style.

\item[\texttt{mhchem}] This package is useful to correctly typeset chemical 
compounds and reactions in a more author-friendly way than resorting to math 
mode: to write $\mathrm{H}_3\mathrm{PO}_4$ you write \verb|\ce{H3PO4}| instead
of \verb|$\mathrm{H}_3\mathrm{PO}_4$|.
\end{itemize}

\section*{Template}
\begin{verbatim}
\documentclass{ntnuthesis}

\title{Planetary salutations}
\subtitle{How to greet entire worlds}
\author{John Doe}

\faculty{Faculty of Astronomy}
\department{Department of Planetary Studies}
\setpublisher{some fine publisher}

\begin{document}
\frontmatter
\maketitle
\tableofcontents

\mainmatter
\chapter{Introduction}
Hello World!

$$ E=mc^2 $$

\appendix
\chapter{Salutations}
Goodbye World!

\backmatter
\chapter{Index}
Hello, goodbye.

\end{document}
\end{verbatim}


\section*{Licence (GPL v3)}
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see 
\href{http://www.gnu.org/licenses/}{http://www.gnu.org/licenses/}.

\end{document}
