% This class is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
%
% File for the ntnuthesis LaTeX class.
% This class produces a subset of book suitable for doctoral
% theses written at NTNU.
%
% © Federico Zenith, 2006-2011

\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{ntnuthesis}[2011/10/06 Class for PhD theses at NTNU Trondheim]

% Set default values for the variables used by our implementation of \maketitle
\newcommand{\SubTitle}{}
\newcommand{\DegreeType}{philosophiæ doctor}
\newcommand{\Faculty}{}
\newcommand{\Department}{}
\newcommand{\CopyrightNotice}{All rights reserved.}
\newcommand{\ISBNPrinted}{N/A}
\newcommand{\ISBNElectronic}{N/A}
\newcommand{\SerialNumber}{\number\year:N/A}
\newcommand{\Month}{\ifcase\month
                      \or January
                      \or February
                      \or March
                      \or April
                      \or May
                      \or June
                      \or July
                      \or August
                      \or September
                      \or October
                      \or November
                      \or December
                    \fi}
\newcommand{\Year}{\number\year}
\newcommand{\Publisher}{}

% Set the default values for margin settings
\newcommand{\GeometryOptions}{margin=20mm,top=15mm,bindingoffset=10mm}
\newcommand{\CropOptions}{}
\newcommand{\CropLandscapeFlag}{}

% Convenience command to give two different versions of code, the first for
% printout mode and the second for screen mode.
% Useful for large pictures and layouts in absolute units.
\RequirePackage{etoolbox}
\newcommand{\printandscreen}{\ifundef{\ScreenMode}}

% Process options passed to the class
\DeclareOption{a4crop}{\renewcommand{\CropOptions}{cam,a4,center}}
\DeclareOption{draft} {\PassOptionsToClass{\CurrentOption}{book}}
\DeclareOption{final} {\PassOptionsToClass{\CurrentOption}{book}}
\DeclareOption{screen}{
    \renewcommand{\CropLandscapeFlag}{landscape}
    \renewcommand{\GeometryOptions}{margin=10mm,screen}
    \newcommand{\ScreenMode}{}
}
\DeclareOption*{\PackageWarning{ntnuthesis}{Unknown option `\CurrentOption´.}}
\ProcessOptions\relax

\LoadClass[b5paper,11pt,twoside,openright,onecolumn,titlepage]{book}

% Set appropriate margins
\RequirePackage[\GeometryOptions,includeheadfoot,headheight=14.5pt]{geometry}

% Set cropping marks, if a4crop has been specified.
\RequirePackage[\CropOptions,\CropLandscapeFlag]{crop}

% Necessary to set the headers
\RequirePackage{fancyhdr}
% Necessary to include the NTNU logo
\RequirePackage{graphicx}
% fontspec helps with font and Unicode support
\RequirePackage{fontspec}
% Activate the standard TeX ligatures
\defaultfontfeatures{Ligatures=TeX}
% TeX Gyre Termes is a Times clone
\setmainfont{TeX Gyre Termes}

% Define the commands to set the variables used by \maketitle
\newcommand{\subtitle}{\renewcommand{\SubTitle}}
\newcommand{\degreetype}{\renewcommand{\DegreeType}}
\newcommand{\faculty}{\renewcommand{\Faculty}}
\newcommand{\department}{\renewcommand{\Department}}
\newcommand{\copyrightnotice}{\renewcommand{\CopyrightNotice}}
\newcommand{\isbnprinted}{\renewcommand{\ISBNPrinted}}
\newcommand{\isbnelectronic}{\renewcommand{\ISBNElectronic}}
\newcommand{\serialnumber}{\renewcommand{\SerialNumber}}
\newcommand{\setyear}{\renewcommand{\Year}}
\newcommand{\setmonth}{\renewcommand{\Month}}
\newcommand{\setpublisher}{\renewcommand{\Publisher}}

% Lay out the title and colophon pages
\renewcommand{\maketitle}{
    \begin{titlepage}
    \sffamily
    \parindent=0cm
    \addtolength{\parskip}{\baselineskip}
    {\Huge \@author}
    \printandscreen{\vspace{2cm}}{\vspace{0.5cm}}

    {\Huge \textbf{\@title}}

    {\LARGE \SubTitle}
    \vfill

    {\large Doctoral thesis\\for the degree of \DegreeType

    Trondheim, \Month\ \Year

    Norwegian University of Science and Technology\\
    \Faculty\ \\
    \Department}
    \printandscreen{\vspace{2cm}}{\vspace{0.75cm}}

    \includegraphics[width=0.4\textwidth]{NTNUlogo}

    \newpage % Start colophon page here
    \vspace*\fill
    \begin{scriptsize}
      \textbf{NTNU}

      Norwegian University of Science and Technology

      Doctoral thesis\\
      for the degree of \DegreeType

      \Faculty\ \\
      \Department

      \copyright\ \Year\ \@author. \CopyrightNotice

      ISBN \ISBNPrinted\ (printed version)\\
      ISBN \ISBNElectronic\ (electronic version)\\
      ISSN 1503-8181

      Doctoral theses at NTNU, \SerialNumber

      Printed by \Publisher
    \end{scriptsize}
    \end{titlepage}
}

% Setup the headers
\AtBeginDocument{
    \pagestyle{fancy}
    \renewcommand{\headrulewidth}{0.4pt}
    \renewcommand{\sectionmark}[1]{\markright{\textbf{\thesection.\ #1}}}
    \renewcommand{\chaptermark}[1]{\markboth{\textbf{#1}}{}}
    \rhead{\nouppercase{\leftmark}}
    \lhead{\nouppercase{\rightmark}}
    \printandscreen{
        \fancyhead[LE,RO]{\textbf{\thepage}}
    }{
        \fancyhead[R]{\textbf{\thepage}}
    }
    \fancyfoot[C]{}
}
